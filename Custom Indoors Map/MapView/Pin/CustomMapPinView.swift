//
//  CustomMapPinView.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-09-01.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit
import SDWebImage


class CustomMapPinView: UIView {

    @IBOutlet var aligmentFrame: UIView!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var avatarImageView: UIImageView!
    
    @IBOutlet var counterContainer: UIView!
    @IBOutlet var counterLabel: UILabel!
    
    
    private(set) var pin: CustomMapPinEntity!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        translatesAutoresizingMaskIntoConstraints = false
        counterContainer.layer.cornerRadius = 22/2
    }

    func populate(pin: CustomMapPinEntity) {
        self.pin = pin
        iconImageView.image = pin.type.image()
        widthConstraint.constant = pin.size.width
        heightConstraint.constant = pin.size.height
        if let url = pin.imageUrl {
            avatarImageView.sd_setImage(with: url)
        } else {
            avatarImageView.image = nil
            avatarImageView.isHidden = true
        }
    }
}
