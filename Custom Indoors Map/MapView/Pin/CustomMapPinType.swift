//
//  CustomMapPinType.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-08-31.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit


enum CustomMapPinType: CaseIterable {
    case deskOccupied
    case deskFree
    case deskUnlinked
    case roomOccupied
    case roomFree
    case roomUnlinked
    
    func image() -> UIImage? {
        var name: String!
        switch self {
            
        case .deskOccupied:
            name = "desk_occupied"
        case .deskFree:
            name = "desk_unoccupied"
        case .deskUnlinked:
            name = "desk_unlinked"
        case .roomOccupied:
            name = "room_occupied"
        case .roomFree:
            name = "room_unoccupied"
        case .roomUnlinked:
            name = "room_unlinked"
        }
        
        return UIImage(named: name)
    }
    
    static func randomType() -> CustomMapPinType {
        return allCases.randomElement()!
    }
}
