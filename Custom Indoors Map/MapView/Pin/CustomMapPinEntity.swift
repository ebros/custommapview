//
//  CustomMapPin.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-08-31.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit


class CustomMapPinEntity {
    let x: CGFloat
    let y: CGFloat
    
    let size: CGSize
    
    var type: CustomMapPinType = .deskFree
    var imageUrl: URL?
    
    init(x: CGFloat,
         y: CGFloat,
         size: CGSize = CGSize(width: 32, height: 32),
         type: CustomMapPinType,
         imageUrl: URL? = nil) {
        
        self.x = x
        self.y = y
        self.size = size
        self.type = type
        self.imageUrl = imageUrl
    }
}
