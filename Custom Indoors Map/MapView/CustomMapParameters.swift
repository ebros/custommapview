//
//  CustomMapParameters.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-08-31.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit


struct CustomMapParameters {
    let height: CGFloat
    let width: CGFloat
    let padding: UIEdgeInsets
    let minZoom: CGFloat
    let maxZoom: CGFloat
}
