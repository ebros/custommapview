//
//  CustomMapView.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-08-31.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit
import SDWebImage
import SDWebImageSVGCoder


class CustomMapView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var mapImageView: UIImageView!
    
    @IBOutlet private var mapImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var mapImageWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet private var mapLeadingPaddingConstraint: NSLayoutConstraint!
    @IBOutlet private var mapTrailingPaddingConstraint: NSLayoutConstraint!
    @IBOutlet private var mapBottomPaddingConstraint: NSLayoutConstraint!
    @IBOutlet private var mapTopPaddingConstraint: NSLayoutConstraint!
    
    
    private var pinList: [CustomMapPinEntity] = []
    private var pinViewList: [CustomMapPinView] = []
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        translatesAutoresizingMaskIntoConstraints = false
        scrollView.delegate = self
    }
    
    func populate(svgMapUrl: URL, mapParameters: CustomMapParameters, pinList: [CustomMapPinEntity]) {
        
        mapImageView.sd_setImage(with: svgMapUrl) { (image, error, cacheType, url) in
            debugPrint("svg loaded")
        }
        
        mapImageWidthConstraint.constant = mapParameters.width
        mapImageHeightConstraint.constant = mapParameters.height
        mapLeadingPaddingConstraint.constant = mapParameters.padding.left
        mapTrailingPaddingConstraint.constant = mapParameters.padding.right
        mapBottomPaddingConstraint.constant = mapParameters.padding.bottom
        mapTopPaddingConstraint.constant = mapParameters.padding.top
        
        scrollView.maximumZoomScale = mapParameters.maxZoom
        scrollView.minimumZoomScale = mapParameters.minZoom
        
        drawPins(pinList: pinList)
    }
    
    
    // MARK: - Private
    func drawPins(pinList: [CustomMapPinEntity]) {
        pinViewList.forEach({$0.removeFromSuperview()})
        pinViewList = []
        
        for pin in pinList {
            if let pinView = Bundle.main.loadNibNamed(String(describing: CustomMapPinView.self),
                                                      owner: nil,
                                                      options: nil)?.first as? CustomMapPinView {
                pinView.populate(pin: pin)
                mapImageView.addSubview(pinView)
                
                pinView.centerXAnchor.constraint(equalTo: mapImageView.leadingAnchor,
                                                 constant: pin.x)
                    .isActive = true
                pinView.centerYAnchor.constraint(equalTo: mapImageView.topAnchor,
                                                 constant: pin.y)
                    .isActive = true
                
                pinViewList.append(pinView)
            }
        }
    }
    
    
    
}


// MARK: - UIScrollViewDelegate
extension CustomMapView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("Content offset: \(scrollView.contentOffset)")
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        print("Zoom scale: \(scrollView.zoomScale)")
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return contentView
    }
}

