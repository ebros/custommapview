//
//  ViewController.swift
//  Custom Indoors Map
//
//  Created by Martynas Stanaitis on 2020-08-31.
//  Copyright © 2020 Narbutas. All rights reserved.
//

import UIKit
import PureLayout


class ViewController: UIViewController {

    @IBOutlet var mapContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let mapView = Bundle.main.loadNibNamed(String(describing: CustomMapView.self), owner: nil, options: nil)?.first as? CustomMapView,
            let mapUrl = Bundle.main.url(forResource: "map-floor5", withExtension: "svg") {
            mapContainer.addSubview(mapView)
            mapView.autoPinEdgesToSuperviewEdges()
            
            let parameters = CustomMapParameters(height: 1301,
                                                 width: 988,
                                                 padding: UIEdgeInsets(top: 75, left: 75, bottom: 75, right: 75),
                                                 minZoom: 0.5,
                                                 maxZoom: 2)
            var pinList: [CustomMapPinEntity] = []
            for _ in 0..<500 {
                let pin = CustomMapPinEntity(x: CGFloat.random(in: 0..<parameters.width),
                                             y: CGFloat.random(in: 0..<parameters.height),
                                             type: CustomMapPinType.randomType(),
                                             imageUrl: URL(string: "https://app.flanco.com/api/user/image/e850aac0-dbac-11ea-b191-e9f552a5a808")!)
                pinList.append(pin)
            }
            mapView.populate(svgMapUrl: mapUrl, mapParameters: parameters, pinList: pinList)
        }
    }


}

